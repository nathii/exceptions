package psybergate.grad2019.javafnds.exceptions.hw5;

public class Account {

	private int accountNumber;
	private String name;
	private double accountBalance;
	
	public Account(int accountNumber, String accountHolder, double accountBalance) {
		
		this.accountNumber = accountNumber;
		this.name = accountHolder;
		this.accountBalance = accountBalance;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountHolder() {
		return name;
	}

	public void setAccountHolder(String accountHolder) {
		this.name = accountHolder;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + accountNumber;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accountNumber != other.accountNumber)
			return false;
		return true;
	}
}
