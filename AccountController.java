package psybergate.grad2019.javafnds.exceptions.hw5;

public class AccountController {

	private AccountService service;

	public AccountController() {
		service = new AccountService();
	}

	/**
	 * Create a new account
	 * 
	 * @param newAccountString
	 * @return
	 * @throws InvalidInputStringException
	 * @throws FailedAccountCreationException
	 */
	public void addAccount(String newAccountString) throws InvalidInputStringException, FailedAccountCreationException {

		String[] tokenizedInputs = tokenizeUserInputs(newAccountString);

		try {

			Integer accNum = Integer.parseInt(tokenizedInputs[0]);
			Double openingBalance = Double.parseDouble(tokenizedInputs[2]);

			boolean success = service.addAccount(new Account(accNum, tokenizedInputs[1], openingBalance));
			if (success) {
				System.out.println("\n => Account #" + accNum + " successfully added!");
			}
		} catch (NumberFormatException ex) {
			throw new InvalidInputStringException(ex);
		} catch (FailedAccountCreationException ex) {
			throw ex;
		} catch (AccountNumberExistsException ex) {
			throw new FailedAccountCreationException(ex);
		}
	}

	/**
	 * Make a withdrawal
	 * 
	 * @param inputString
	 * @return
	 * @throws InvalidInputStringException
	 */
	public String withdraw(String inputString) throws InvalidInputStringException, FailedWithdrawalException {

		String[] tokenizedInputs = tokenizeUserInputs(inputString);

		try {

			Integer accNum = Integer.parseInt(tokenizedInputs[0]);
			Double amount = Double.parseDouble(tokenizedInputs[1]);

			if (service.withdraw(accNum, amount)) {
				return ("\n => Withdrawn R" + amount + " from #" + accNum);
			}
		} catch (NumberFormatException ex) {
			throw new InvalidInputStringException(ex);
		} catch (FailedWithdrawalException ex) {
			throw ex;
		}

		return "";
	}

	/**
	 * Check account balance
	 * 
	 * @param inputString
	 * @return
	 * @throws InvalidInputStringException
	 */
	public double viewBalance(String inputString) throws InvalidInputStringException, AccountNotFoundException {

		double balance = 0;
		String[] tokenizedInputs = tokenizeUserInputs(inputString);

		try {

			Integer accNum = Integer.parseInt(tokenizedInputs[0]);
			balance = service.viewBalance(accNum);
		} catch (NumberFormatException ex) {
			throw new InvalidInputStringException(ex);
		} catch (AccountNotFoundException ex) {
			throw ex;
		}

		return balance;
	}

	/**
	 * Tokenize input string into tokens
	 * 
	 * @param inputString
	 * @return
	 */
	private String[] tokenizeUserInputs(String inputString) {
		return inputString.split(",");
	}
}
