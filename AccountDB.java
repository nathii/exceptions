package psybergate.grad2019.javafnds.exceptions.hw5;

import java.util.ArrayList;
import java.util.Collection;

public class AccountDB {

	private Collection<Account> accounts;

	AccountDB() {
		this.accounts = new ArrayList<>();
	}
	
	public boolean addAccount(Account account) {
		return accounts.add(account);
	}
	
	public boolean withdraw(int accNum, double amount) {
		Account account = getAccountByAccountNumber(accNum);
		account.setAccountBalance(account.getAccountBalance() - amount);
		return true;
	}
	
	public Account getAccountByAccountNumber(int accNum) {
		
		for (Account account : accounts) {
			if(account.getAccountNumber() == accNum) {
				return account;
			}
		}
		
		return null;
	}
}
