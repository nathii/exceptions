package psybergate.grad2019.javafnds.exceptions.hw5;

public class AccountNotFoundException extends Exception{

	public AccountNotFoundException(String message) {
		super(message);
	}
}
