package psybergate.grad2019.javafnds.exceptions.hw5;

public class AccountNumberExistsException extends Exception {
	
	private static final String message = "Your chosen account number already exists.";

	public AccountNumberExistsException() {
		super(message);
	}
}
