package psybergate.grad2019.javafnds.exceptions.hw5;

public class AccountService {

	private AccountValidator validator;
	private AccountDB db;

	public AccountService() {
		db = new AccountDB();
		validator = new AccountValidator();
	}

	/**
	 * Create a new account
	 * 
	 * @param account
	 * @return
	 * @throws FailedAccountCreationException
	 * @throws AccountNumberExistsException
	 */
	public boolean addAccount(Account account) throws FailedAccountCreationException, AccountNumberExistsException {

		try {

			if (validator.validateAccountNumber(account.getAccountNumber()) && 
				validator.validateAccountOpeningBalance(account.getAccountBalance())) {

				if(db.getAccountByAccountNumber(account.getAccountNumber()) != null) {
					throw new AccountNumberExistsException();
				}
				
				db.addAccount(account);
			}
		} 
		catch (InvalidAccountNumberException | InsufficientOpeningBalanceException ex) {
			throw new FailedAccountCreationException(ex);
		} 
		catch (AccountNumberExistsException ex) {
			throw ex;
		}
		
		return true;
	}

	/**
	 * Make a withdrawal
	 * 
	 * @param accountNumber
	 * @param amount
	 * @throws FailedWithdrawalException
	 */
	public boolean withdraw(int accountNumber, double amount) throws FailedWithdrawalException {
		
		try {
			
			Account account = db.getAccountByAccountNumber(accountNumber);
			
			if(account == null) {
				throw new AccountNotFoundException("Account #" + accountNumber + " does not exist.");
			}
			else {
				if(validator.validateAccountNumber(accountNumber) &&
				   validator.validateWithdrawal(amount, account.getAccountBalance())) {
					   db.withdraw(accountNumber, amount);
				}
			}
		}
		catch(InvalidAccountNumberException | AccountNotFoundException | InvalidWithdrawalAmountException ex) {
			throw new FailedWithdrawalException(ex);
		}
		
		return true;
	}

	/**
	 * Retrieve account balance
	 * 
	 * @param accountNumber
	 * @return
	 * @throws AccountNotFoundException
	 */
	public double viewBalance(int accountNumber) throws AccountNotFoundException {
		
		Account account = db.getAccountByAccountNumber(accountNumber);
		if(account != null) {
			return account.getAccountBalance();
		}
		
		throw new AccountNotFoundException("Account #" + accountNumber + " does not exist.");
	}
}
