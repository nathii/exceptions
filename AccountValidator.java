package psybergate.grad2019.javafnds.exceptions.hw5;

public class AccountValidator {

	private static double minOpeningBalance = 1000;
	private static double minWithdrawalAmount = 500;

	public AccountValidator() {

	}

	/**
	 * Validate opening account balance
	 * 
	 * @param openingBalance
	 * @return
	 * @throws InsufficientOpeningBalanceException
	 */
	public boolean validateAccountOpeningBalance(double openingBalance) throws InsufficientOpeningBalanceException {
		if (openingBalance >= minOpeningBalance) {
			return true;
		}

		throw new InsufficientOpeningBalanceException("Opening account balance must be atleast R1000.00");
	}

	/**
	 * Validate account number
	 * 
	 * @param accountNumber
	 * @return
	 * @throws InvalidAccountNumberException
	 */
	public boolean validateAccountNumber(int accountNumber) throws InvalidAccountNumberException {
		if ((accountNumber % 11 == 0) && checkAccountNumberLength(accountNumber)) {
			return true;
		}

		throw new InvalidAccountNumberException();
	}

	public boolean validateWithdrawal(double amount, double accountBalance) throws InvalidWithdrawalAmountException {
		if (amount >= minWithdrawalAmount && amount <= accountBalance) {
			return true;
		}

		throw new InvalidWithdrawalAmountException();
	}

	/**
	 * Check length of account-number
	 * 
	 * @param accNum
	 * @return
	 */
	private boolean checkAccountNumberLength(int accNum) {
		return (Integer.valueOf(accNum).toString().length() >= 3);
	}
}
