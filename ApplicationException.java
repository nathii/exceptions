package psybergate.grad2019.javafnds.exceptions.hw5;

public class ApplicationException extends Exception {

	public ApplicationException(String message, Throwable cause) {
		super(message, cause);
	}
}
