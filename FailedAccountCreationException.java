package psybergate.grad2019.javafnds.exceptions.hw5;

public class FailedAccountCreationException extends ApplicationException {
	
	private static final String message = "Account creation has failed";

	public FailedAccountCreationException(Throwable cause) {
		super(message, cause);
	}
}
