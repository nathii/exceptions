package psybergate.grad2019.javafnds.exceptions.hw5;

public class FailedWithdrawalException extends Exception {
	
	private static final String message = "Withdrawal unsuccessful.";

	public FailedWithdrawalException(Throwable cause) {
		super(message, cause);
	}
}
