package psybergate.grad2019.javafnds.exceptions.hw5;

public class InsufficientOpeningBalanceException extends Exception {

	public InsufficientOpeningBalanceException(String message) {
		super(message);
	}
}
