package psybergate.grad2019.javafnds.exceptions.hw5;

public class InvalidAccountNumberException extends Exception {
	
	private static final String message = "Account number must be a 3 digit integer (min) divisible by 11.";

	public InvalidAccountNumberException() {
		super(message);
	}
}
