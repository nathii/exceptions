package psybergate.grad2019.javafnds.exceptions.hw5;

public class InvalidInputStringException extends Exception{

	private static final String message = "Expected input: <3 digit Integer divisible by 11>, "
			   							+ "<Name>, "
			   							+ "<Opening balance, minimum of R1000>";
	
	public InvalidInputStringException(Throwable cause) {
		super(message, cause);
	}
}
