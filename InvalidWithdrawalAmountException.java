package psybergate.grad2019.javafnds.exceptions.hw5;

public class InvalidWithdrawalAmountException extends Exception {
	
	private static final String message = "Minimum withdrawal amount is R500.00 AND balance must be larger than withdrawal amount.";

	public InvalidWithdrawalAmountException() {
		super(message);
	}
}
