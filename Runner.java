package psybergate.grad2019.javafnds.exceptions.hw5;

import java.util.Scanner;

public class Runner {

	private static AccountController controller = new AccountController();
	
	public static int makeSelection(Scanner scan) {

		System.out.println("\n1. Add account");
		System.out.println("2. Withdraw");
		System.out.println("3. View balance");
		System.out.println("4. Exit");

		System.out.print("\nSelect: ");
		return scan.nextInt();
	}

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		do {

			int selected = Runner.makeSelection(scan);
			System.out.println("\n--------------------------------------------------------------");

			String inputString = "";

			switch (selected) {
			case 1: {

				try {

					System.out.print("\nCREATE ACCOUNT >> Enter input string: ");
					inputString = scan.next();
					controller.addAccount(inputString);
				} 
				catch (InvalidInputStringException ex) {
					System.out.println("\nInvalidInputStringException: " + ex.getMessage());
					System.out.println("Cause: " + ex.getCause().getMessage());
				} 
				catch (FailedAccountCreationException ex) {
					System.out.println("\nFailedAccountCreationException: " + ex.getMessage());
					System.out.println("Cause: " + ex.getCause().getMessage());
				}

				break;
			}

			case 2: {

				try {

					System.out.print("\nWITHDRAW >> Enter input string: ");
					inputString = scan.next();
					System.out.println(controller.withdraw(inputString));
				} 
				catch (InvalidInputStringException ex) {
					System.out.println("\nInvalidInputStringException: " + ex.getMessage());
					System.out.println("Cause: " + ex.getCause().getMessage());
				} 
				catch (FailedWithdrawalException ex) {
					System.out.println("\nFailedWithdrawalException: " + ex.getMessage());
					System.out.println("Cause: " + ex.getCause().getMessage());
				}

				break;
			}

			case 3: {

				try {
					System.out.print("\nBALANCE >> Enter account-number: ");
					inputString = scan.next();
					System.out.println("\n => Account balance: " + Runner.controller.viewBalance(inputString));
				} 
				catch (InvalidInputStringException ex) {
					System.out.println("\nInvalidInputStringException: " + ex.getMessage());
					System.out.println("Cause: " + ex.getCause().getMessage());
				} 
				catch (AccountNotFoundException ex) {
					System.out.println("\nAccountNotFoundException: " + ex.getMessage());
				}

				break;
			}

			case 4: {

				System.out.println("-------------------> Transaction complete <-------------------");
				System.exit(0);
				break;
			}
			}
		} while (true);
	}
}
